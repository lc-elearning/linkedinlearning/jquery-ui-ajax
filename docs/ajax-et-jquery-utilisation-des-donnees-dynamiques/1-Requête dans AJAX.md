## Présentation de la formation AJAX et jQuery
La fonction `$.load()` permet de charger un document, ou une partie de ce document au sein d'un élément du DOM.

## L'approche rapide $.load
```javascript
$(body).load('index.html'); //Permet de charger le fichier index.html au sein de body.
$(body).load('index.html', checkChargement); //Permet de charger le fichier index.html au sein de body et de lancer la fonction checkChargement
$(body).load('index.html #a'); //Permet de charger l'élément identifié par l'id a au sein du fichier index.html
$(body).load('index.php?info=a'); //Permet de charger l'élément renvoyé par le fichier PHP index.php lorsque l'on passe par $_GET['info'] = a
$(body).load('index.php', {info = 'a'}); //Permet de charger l'élément renvoyé par le fichier PHP index.php lorsque l'on passe par $_POST['info'] = a
```

## Les alternatives $.post et $.get
L'utilisation des méthodes `$.get(file, data, callback)` et `$.post(file, data, callback)` permettent de faire la même chose grâce à jQuery.

## $.ajax en mode abrégé
La requête ajax prend en seul paramètre un objet qui contient différents éléments dont **url** qui est le seul élément obligatoire, qui indique la cible. A la fin de cette requête on peut directement appelé ma la méthode `.done()` qui va effectuer une action des que la requête est terminée.

```javascript
$(document).ready(function(e) {
	$.ajax({
		url:'501-02-3.html'
	}).done(function(arg){
			//console.log('chargement effectué depuis fct')
			console.log(arg)
		}
	)
});
```

## Les diverses fonctions de rappel
Ils en existent 3 principales:

- `.done(function( data, textStatus, jqXHR ) {})`: effectuer lors du succès de l'opération;
- `.fail(function( jqXHR, textStatus, errorThrown ) {})`: effectuer lors de l'échec de l'opération;
- `.always(function( data|jqXHR, textStatus, jqXHR|errorThrown ) {})`: effectuer à la fin de l'opération, peut importe le statut;

## Les propriétés de la requête
On peut ajouter la propriété `succes` à l'objet ajax qui représente une fonction à appeler lors du succès de la requête.

La propriété `beforeSend` définit une fonction à appeler avant d'appeler la requête. Cela va changer uniquement la requête.

```javascript
$(document).ready(function(e) {
	$.ajax({
		url:'501-02-5.html',
		success:chargementOK,
		beforeSend : function(xhr){
			xhr.overrideMimeType("text/plain; charset=iso-8859-1")
		}
	})
});
function chargementOK(arg){
	console.log(arg)
}
```
