## Les divers évènements d'AJAX
On différencie les évènemnts AJAX qui sont liés à jQuery, et les évènements AJAX Global qui sont liés au document.

On peut prendre en exemple la fonction `.ajaxSuccess()` qui permet de gérer la réussite d'un évènement de manière globale. Cette fonction prend 3 paramètres falcultatifs:

- **evt**: l'évènement intercepté;
- **xhr**: la requête HTTP;
- **settings**: les paramètres de la requête AJAX (url, etc.)

```javascript
$(document).ajaxSuccess(function(evt, xhr, settings){
	console.log("evenement global Ajax")
	console.log(settings.url)
})
$(document).ready(function(e) {
	$('body').load('501-03-1.html',chargementOK)
	//$.get('501-03-1.html',chargementOK)
});
function chargementOK(arg){
	console.log(arg)
}
```

## ajaxStart(), ajaxStop()
De manière transparente, ces fonctions permettent d'intercépter des évènements AJAX globaux, lorsqu'ils commencent et lorsqu'ils sont terminés. L'ordre d'appel est *start*, *omplete*, *stop*.

Il n'y a pas de fonction *Progress* car AJAX n'a pas vocation à chargé des éléments lourds. Si on souhaite mettre une animationde chargement, on peut la démarrer avec `ajaxStart()` et la terminer avec `ajaxStop()`.

## sucess vs complete
*sucess* se traduit par une réussite de la requête alors que *complete* est appelé lors de la fin de la requête peut importe le résultat de la requête. Lors d'un échec on utlise *error*.

Il ne faut pas confondre les fonctions de rappels placées en propriétés de l'objet AJAX avec les fonctions callback qui sont placés sur des méthodes de l'objet XHR.

L'ordre d'exécution *error*, *always*, *fail*, *complete* et *sucess*, *done*, *always*, *complete*.

## La délégation
Si une fonction porte sur les éléments d'un conteneur et que l'on ajoute des nouveaux éléments à celui-ci, alors ces nouveaux éléments ne seront pas pris en compte par la fonciton.

POur que les éléments soient pris en compte par la fonction, il faut déléguer la gestion de l'événement au conteneur, qui lui pourra prendre en compte les nouveaux éléments.

```javascript
 $(document).ready(function(e) {
	/*$('li').click(function(){
		console.log($(this).text()) //Fonction portant sur l'élément directement
	})*/
	$('.liste').on('click','li',fctAppel) //Fonction portant sur le conteneur qu iva capturer les éléments de ses enfants
	$.ajax({
		url:'501-03-4.html',
		success : ajouteListe
	})
})
function ajouteListe(arg){
	$('.liste').append(arg)
}
function fctAppel(){
	console.log($(this).text())
}
```
