## Encodage des fichiers 
Grâce à AJAX il est possible de spécifier le type MIME et l'encodage du document chargé comme suit:
```javascript
$(document).ready(function(e) {
	$.ajax({
		url :'501-04-21.php',
		success : fctSuccess
		//, mimeType : "text/plain; charset=iso-8859-1" // pour 23.php
		//, mimeType : "text/xml; charset=iso-8859-1" // pour 23.php
		/*, beforeSend : function(xhr){
			xhr.overrideMimeType("text/xml; charset=iso-8859-1") // pour 25.php
		}*/
	})
})
function fctSuccess(arg){
	console.log(arg)
}
```
On peut également voir que le type MIME peut être écrasé par la fonction dans le `beforeSend`.

## setRequestHeader
Cette méthode des objets XHR permet de définir des propriétés dans le header de la requête.
```javascript
$(document).ready(function(e) {
	$.ajax({
		url :'501-04-31.php',
		success : fctSuccess,
		beforeSend : function(xhr){
			xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded")
			xhr.setRequestHeader("Source-Expedition","Salon de nouvelles technologies")
		}
	})
})
function fctSuccess(arg){
	console.log(arg)
}
```
On peut récupérer les infos avec le code PHP suivant:
```php
<?php
	//echo 'retour';
	$recup = apache_request_headers();
	echo $recup['Source-Expedition']
?>
```

## getResponseHeader
D'une manière similaire, on peut faire la même chose avec le header de la réponse du serveur.
```javascript
$(document).ready(function(e) {
	$.ajax({
		url :'501-04-41.php',
		success : fctSuccess
	})
})
function fctSuccess(arg, status, xhr){
	//console.log(xhr.getResponseHeader('Content-Type'))
	console.log(xhr.getResponseHeader('Serveur-Distribution'))
}
```

## Gérer les erreurs
La fonction callback appelé par `.fail()` prend 3 argumetns: *xhr*, *status* (erreur, sucess), *error* (message d'erreur). On va utiliser `xhr.status`pour récupérer le code l'erreur. On peut utiliser le même genre de code pour la fonction callback de la propriété *error* de l'objet AJAX.
```javascript
$(document).ready(function(e) {
	var fctAjax = $.ajax({
		url :'501-04-51.php'
		//, error : fctError
	})
	fctAjax.fail(fctFail)
})
function fctError(xhr, status, erreur){
	console.log(xhr.status)
	console.log(status)
	console.log(erreur)
}
function fctFail(xhr, status, erreur){
	console.log('retour de fctFail')
	console.log(xhr.status)
	console.log(status)
	console.log(erreur)
}
```

## $.ajaxSetup()
Cette fonction permet de définir les propriétés par défaut pour les futures requêtes AJAX.

